package com.in28minutes.springboot.web.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.in28minutes.springboot.web.model.Todo;
import com.in28minutes.springboot.web.service.TodoRepository;

@RestController
@RequestMapping(path = "/restful", produces = "application/json")
public class TodoRestController {

	@Autowired
	TodoRepository todoRepo;

	@GetMapping("/{user}")
	public List<Todo> getTodos(@PathVariable String user) {
		return todoRepo.findByUser(user);
	}

	@GetMapping("/list")
	public List<Todo> listTodos() {
		return todoRepo.findAll();
	}

	@GetMapping("todos/{id}")
	public ResponseEntity<Todo> findTodo(@PathVariable int id) {
		Optional<Todo> optTodo = todoRepo.findById(id);
		if (optTodo.isPresent()) {
			return new ResponseEntity<Todo>(optTodo.get(), HttpStatus.OK);

		}

		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

}
